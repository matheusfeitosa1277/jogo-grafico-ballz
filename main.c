#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <stdio.h>
#include "structs.h"

/*
  Cores definidas em "struct.h" : CHUD, CMAP, WHITE, YELLOW, BLACK
*/

enum {MENU, SHOP, PLAYING, ROUND, END, PAUSE, RESTART} estado;

int main(){
    estado = MENU;
     /*
        INICIALIZANDO O ALLEGRO E LOOP PRINCIPAL
        OBS - al_load_... e al_create_... precisam ser destruidos para liberar a memoria
     */
    int width = 490, height = 980; //Proporcao 14/7 -> 2/1
    int running = 1;

    al_init();
    al_install_mouse();
    al_init_font_addon();
    al_init_ttf_addon();

    al_install_audio();
    al_init_acodec_addon();//.wav
    al_reserve_samples(30);//Maximos de sons


    al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST); //Multisampling
    al_set_new_display_option(ALLEGRO_SAMPLES       , 8, ALLEGRO_SUGGEST); //Anti aliasing

    ALLEGRO_DISPLAY *disp = al_create_display(width, height);
    ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
    ALLEGRO_TIMER *timer = al_create_timer(1.0/60.0);           //60 ticks por segundo

    ALLEGRO_MOUSE_STATE state;
    ALLEGRO_EVENT event;
    ALLEGRO_FONT *font = al_create_builtin_font();
    ALLEGRO_FONT *bigFont = al_load_font("resources/roundBold.ttf", 30, 0);

    al_register_event_source(queue, al_get_timer_event_source(timer));
    al_register_event_source(queue, al_get_display_event_source(disp));
    al_register_event_source(queue, al_get_mouse_event_source());
    al_start_timer(timer);

    bool redraw = true;

    /*
        INICIALIZANDO VARIAVEIS DO PLAYING
    */

    //INICIALIZANDO A MATRIZ PRINCIPAL - Fila Circular
    struct entry *n1, *n2;
    struct circlehead head;
    int tamCircleq = 0;
    srandom(time(NULL));
    CIRCLEQ_INIT(&head);

     
    //Variavies de suporte
    int n, aux, anyp = 0, playing = 0; //Variaveis do PLAYING
    int flock = 1;
    float dX, dY = -1;
    float a, b;
    int throwing = 0, tim, nb; //Variaveis do THROWING

    player *comp = NULL;
    circle *ball;

    /*
       DECLARANDO VARIAVEIS DO ROUND
    */
    int offset;


    /*
        MAIN LOOP - Inicio do jogo
    */
    while (running) {
        al_wait_for_event(queue, &event);
        if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) running = 0;

        if (estado == MENU) {
            switch (event.type){
                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:    //Botoes
                    al_get_mouse_state(&state);
                    if (state.x > 105 && state.x < 385){
                        if (state.y > 490 && state.y < 540) {
                            if (comp == NULL){
                                comp = initPlayer();
                                offset = 0;
                            }
                            estado = ROUND;
                        }
                        else if (state.y >590 && state.y < 640){
                            estado = SHOP; 
                            if (comp == NULL){
                                comp = initPlayer();
                                offset = 0;
                            }
                       }
                    } break;
            }

            if (al_is_event_queue_empty(queue)){ //Desenho Menu
                drawMenu(bigFont); 
                al_flip_display();
                redraw = false;
            }

        }

        else if (estado == SHOP) {
            switch(event.type){
                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                    al_get_mouse_state(&state);
                    if (state.x > 105 && state.x < 385 && state.y > 560 && state.y < 610)
                        estado = MENU;
                    else if (state.y > 310 && state.y < 360){
                        if (state.x > 140+10 && state.x < 210-10) comp->color = RED;
                        if (state.x > 210+10 && state.x < 280-10) comp->color = WHITE;
                        if (state.x > 280+10 && state.x < 350-10) comp->color = CYAN;
                    }

                    break;
            }
            
            if (al_is_event_queue_empty(queue)){
                drawShop(bigFont, comp);
                al_flip_display();
            }

        }

        else if (estado == PAUSE) {
            switch (event.type){
                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                    al_get_mouse_state(&state);
                    if (state.x > 105 && state.x < 385){
                    if (state.y > 420 && state.y < 470) estado = PLAYING;
                        else if (state.y > 490 && state.y < 540) estado = RESTART;
                        else if (state.y > 560 && state.y < 610) estado = MENU;
                        redraw = true; al_flush_event_queue(queue); 
                    } break;
            }

            if (al_is_event_queue_empty(queue)){
                drawPause(bigFont);
                al_flip_display();
            }

        }

        if (estado == RESTART){
            //Matriz e jogadores
            n1 = CIRCLEQ_FIRST(&head);
            while(n1 != (void *)&head) {
                n2 = CIRCLEQ_NEXT(n1, entries);
                freeRow(n1);
                n1 = n2; tamCircleq --;
            }
            CIRCLEQ_INIT(&head);//Reinicializa a matriz
            //Salva
            ALLEGRO_FILE *file = al_fopen("resources/player.sav", "w");
            if (file) {
                char buf[1024];
                sprintf(buf, "%d", comp->highScore); al_fputs(file, buf); al_fputc(file, '\n');
                sprintf(buf, "%d", comp->coins); al_fputs(file, buf);
                al_fclose(file);
            }
            comp = freePlayer(comp);

            estado = ROUND;
            comp = initPlayer();
            offset = 0; redraw = true; playing = 0;
            al_flush_event_queue(queue);
        }

        else if (estado == ROUND){                   //Entre as Rodadas
            if (offset == 0){
            n1 = initRow(comp->score);               //Inicializa uma linha
            CIRCLEQ_INSERT_TAIL(&head, n1, entries); //Insere
            tamCircleq++;
            drawMatrix0(&head, font);
            al_flip_display();
            }

            //Animacao das matrizes
            offset = 0;
            while (offset < 70){                    //Um espaco de distancia na matriz
                drawHud(bigFont, comp);
                updateMatrixOffset(&head, font, offset);
                al_draw_filled_circle(comp->x, comp->y-10, 10, comp->color);
                al_flip_display();
                offset++;
            }
             
            //Verificao derrota
            if (tamCircleq > 7) {
                n1 = CIRCLEQ_FIRST(&head);
                for (aux=0; aux<7; aux++){
                    if (n1->row[aux] > 0){ //Checa se algum elemento da linha eh um bloco
                        estado = END;
                        break;
                    }
                } 
                CIRCLEQ_REMOVE(&head, n1, entries);
                freeRow(n1);
                tamCircleq--;
            }

            if (estado == ROUND) estado = PLAYING;
            else { //Desfaz a matriz  - Fim do jogo
                n1 = CIRCLEQ_FIRST(&head);
                while(n1 != (void *)&head) {
                    n2 = CIRCLEQ_NEXT(n1, entries);
                    freeRow(n1);
                    n1 = n2; tamCircleq --;
                }
                estado = END;
                CIRCLEQ_INIT(&head);//Reinicializa a matriz
            }
            al_flush_event_queue(queue); 
        }
        else if (estado == END){
            ALLEGRO_FILE *file = al_fopen("resources/player.sav", "w");
            if (file) {
                char buf[1024];
                sprintf(buf, "%d", comp->highScore); al_fputs(file, buf); al_fputc(file, '\n');
                sprintf(buf, "%d", comp->coins); al_fputs(file, buf);
                al_fclose(file);
            }
            comp = freePlayer(comp);
            estado = MENU;
        }
        else if (estado == PLAYING) {
            switch (event.type){ //Logica
                case ALLEGRO_EVENT_TIMER: 

                    if (throwing) {                             //Lancamento
                        if (nb < comp->nBalls) {                //Uma de cada vez
                            if (tim > 10 ) {                    //A cada X frames;
                                comp->balls[nb]->playing = 1;   //Libera a proxima bolinha
                                tim = 0; nb++;
                            }   tim++;                          
                        } else throwing = 0;                   //Lancou todas as bolinhas
                    }

                    if (playing) {                              //Bolinhas pulando
                        anyp = 0;                               //Suponha que nenhuma bolinha esta jogando
                        for(n=0; n<comp->nBalls; n++){         //Para todas as bolinhas
                            ball = comp->balls[n];
                            if (ball->playing){                //Se jogando, atualiza
                                anyp = 1;
                                //Checa as colisoes - COM TODOS os retangulos e atualiza as bolinhas
                                updateCollision(comp, &head, n);

                                if (updateBall(ball) && !flock){ //Se foi a primeira a cair
                                    flock = 1;
                                    comp->x = ball->x; comp->y = ball->y + ball->radius;//?
                                }
                            }
                                
                            
                        }//checou todas as bolinhas

                        if (!anyp && !throwing){ //Nenhuma bolinha quickando ou sendo jogada - FIM DA RODADA
                            for(n=0; n<comp->nBalls; n++){
                                ball = comp->balls[n];
                                ball->x = comp->x, ball->y = comp->y-10;
                            }
                            al_play_sample(point, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                            comp->score +=1;
                            if (comp->score > comp->highScore)
                                comp->highScore = comp->score;

                            playing = 0;
                            offset = 0;
                            estado = ROUND;
                        }

                    }//Fim playing
                    
                    redraw = true; 
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN: //Botao de pause
                    al_get_mouse_state(&state);
                    if (state.x > 5 && state.x < 35 && state.y > 15 && state.y < 65){
                        estado = PAUSE; redraw = true;
                        ALLEGRO_FILE *file = al_fopen("resources/player.sav", "w");
                            if (file) {
                                char buf[1024];
                                sprintf(buf, "%d", comp->highScore); al_fputs(file, buf); al_fputc(file, '\n');
                                sprintf(buf, "%d", comp->coins); al_fputs(file, buf);
                                al_fclose(file);
                            }
                    }
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_UP:        //Lancamento das bolinhas
                    dY = state.y - comp->y + 10;
                    if (!throwing && !playing && dY > 0) { //Lancamento
                        throwing = 1; playing = 1;         //Inicio de um lancamento, Bolinhas em movimento
                        for(n = 0; n < comp->nBalls; n++){
                            comp->balls[n]->x = comp->x; comp->balls[n]->y = comp->y - 10 - 1;
                            comp->balls[n]->dx = a;        //P(t) = x0 + at (parametrizacao da reta)
                            comp->balls[n]->dy = b;        //P(t) = y0 + bt            """
                        }
                        tim = 0; nb = 0; flock = 0;        //timer, nBall, firstLock;
                    } break;
            }

            if (state.buttons && !playing){  //Calculando a inclinacao da reta
                al_get_mouse_state(&state);
                dX = state.x - comp->x; dY = state.y - comp->y + 10;
                lineCoefficient(state, comp, &a, &b);
            }

            if (redraw && al_is_event_queue_empty(queue)){ //Desenho
                //Interface do jogo
                drawHud(bigFont, comp);

                //Matriz de retangulos e powerUps
                drawMatrix(&head, font);
                
                //Bolinhas
                drawBalls(comp);
                

                //Mira - Semelhanca de Triangulos
                if (state.buttons && !playing && dY > 0) //Triangulo espelhado
                    al_draw_line(comp->x, comp->y -10, comp->x - dX, comp->y - dY, WHITE, 1);

                al_flip_display();
                redraw = false;
            }
        } 
    }//Running
    

    /*
        LIBERANDO MEMORIA
    */

    //Allegro
    al_destroy_display(disp);
    al_destroy_event_queue(queue);
    al_destroy_timer(timer);
    al_uninstall_mouse();
    al_destroy_font(font); al_destroy_font(bigFont);
    al_destroy_sample(pop); al_destroy_sample(point); al_destroy_sample(coin);

    
    //Matriz e jogadores
    n1 = CIRCLEQ_FIRST(&head);
        while(n1 != (void *)&head) {
            n2 = CIRCLEQ_NEXT(n1, entries);
            freeRow(n1);
            n1 = n2; tamCircleq --;
        }
        CIRCLEQ_INIT(&head);//Reinicializa a matriz
    //Salvando
    if (comp != NULL){
    ALLEGRO_FILE *file = al_fopen("resources/player.sav", "w");
    if (file) {
        char buf[1024];
        sprintf(buf, "%d", comp->highScore); al_fputs(file, buf); al_fputc(file, '\n');
        sprintf(buf, "%d", comp->coins); al_fputs(file, buf);
        al_fclose(file);
    } else puts("nao abriu");
    }

    comp = freePlayer(comp);
    
    return 0;
}


#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <allegro5/allegro_primitives.h>

#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/queue.h>

#define pop   al_load_sample("resources/pop.wav")
#define point al_load_sample("resources/point.wav")
#define coin  al_load_sample("resources/coin.wav")

#define CHUD   al_map_rgb( 96,  96,  96) //Cor da HUD
#define CMAP   al_map_rgb( 64,  64,  64) //Cor do Mapa
#define WHITE  al_map_rgb(255, 255, 255) 
#define YELLOW al_map_rgb(255, 255,   0) 
#define ORRANG al_map_rgb(255, 160,   0)
#define BLACK  al_map_rgb(  0,   0,   0) 
#define RED    al_map_rgb(255,   0,  50)
#define BLUE   al_map_rgb(  0, 140, 255)
#define CYAN   al_map_rgb(100, 255, 255)



/*
    Bolinhas
*/
struct circle {
    float x, y;
    float dx, dy;
    float radius;
    int playing;
    ALLEGRO_COLOR color;
} typedef circle;

circle *initBall(int x, int y, int radius);     //Inicializa uma bolinha centralizada

circle *freeBall(circle *ball);                 //Libera memoria de uma bolinha

void setBallXY(circle *ball, float x, float y); //Altera as coordenadas X e Y de uma bolinha

/*
    Jogador
*/

struct player {
    int score, coins;
    circle **balls;
    int nBalls;
    int x, y;
    ALLEGRO_COLOR color;
    int highScore;
} typedef player;


player *initPlayer();       //Inicializa um jogador

player *freePlayer();       //Libera memoria de um jogador

void addBall(player *comp); //Adiciona uma bolinha para o jogador

void addCoin(player *comp); //Adiciona uma moeda """
    
/*  
    Matriz com os retangulos 
    Buffer Circular - Implementacao da <sys/queue.h>
 */

struct entry {
    int *row;
    CIRCLEQ_ENTRY(entry) entries;
};

CIRCLEQ_HEAD(circlehead, entry);

struct entry *initRow(int score); //Inicializa uma linha da matriz de blocos

struct entry *freeRow(struct entry *n); //Libera memoria de """

/*
    Colisao entre elementos
*/

int intersectCircleSquare(circle *ball, float size, float recX, float recY); //Colisao Circulo - Retangulo

int intersectCircle2(circle *ball, int rad, int cX, int cY);                 //Colisao Circulo - Circulo

int intersectWall(circle *ball);                                             //Colisao Circulo - Parede

int updateBall(circle *ball);                                                //Movimenta a bolinha

void updateCollision(player *comop, struct circlehead *head, int n);

/*
    Desenho na matriz ixj
    E importante padronizar os tamanhos de cada elementos
    Blocos = 50x50
    Bolinha = Raio(10)
    Moeda = Raio(20)
    PowerUp = Raio(20)
*/


void drawRectangle(int i, int j, int hp, ALLEGRO_FONT *font);

void drawCoin(int i, int j);

void drawPowerUp(int i, int j);

void drawMatrix(struct circlehead *head, ALLEGRO_FONT *font);

//
void updateMatrixOffset(struct circlehead *head, ALLEGRO_FONT *font, int offSet);
void drawMatrix0(struct circlehead *head, ALLEGRO_FONT *font);
void drawRectangleOffset(int i, int j, int hp, ALLEGRO_FONT *font, int off); //hp = hitPoints
void drawCoinOffset(int i, int j, int off);
void drawPowerUpOffset(int i, int j, int off);
//
/*
    Desenhos fora da matriz
*/

void drawHud(ALLEGRO_FONT *font, player *comp);

void drawBalls(player *comp);

void drawAim();

void drawMenu(ALLEGRO_FONT *font);

void drawPause(ALLEGRO_FONT *font);

void drawShop(ALLEGRO_FONT *font, player *comp);

/*
    Funcoes Auxiliares
*/

void lineCoefficient(ALLEGRO_MOUSE_STATE state, player *comp, float *a, float *b);


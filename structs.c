#include "structs.h"

/*
    Bolinhas
*/

//initBall -> Inicializa uma bolinha de raio "radius"
//Centralizada em x, y.
circle *initBall(int x, int y, int radius) {
    circle *ball = malloc(sizeof(circle)); 
    ball->x = x; ball->y = y; //
    ball->dx = 0; ball->dy = 0;

    ball->radius = radius;
    ball->playing = 0;
    ball->color = WHITE;
    return ball;
}

//freeBAll -> Libera memoria de uma bolinha
circle *freeBall(circle *ball) {
    free(ball);
    return NULL;
}

//setBallXy -> Altera as coordenadas x, y de uma bolinha
void setBallXY(circle *ball, float x, float y){
    ball->x = x; ball->y = y;
}

/*
    Jogador
*/

//initPlayer -> Inicializa um jogadro
player *initPlayer() {
    player *comp = malloc(sizeof(player));
    //Leitura de um save - highScore
    char buf[32];
    ALLEGRO_FILE *file = al_fopen("resources/player.sav", "r");
    if (file) {
        al_fgets(file, buf, 32); sscanf(buf, "%d", &comp->highScore);;
        al_fgets(file, buf, 32); sscanf(buf, "%d", &comp->coins);
        al_fclose(file);
    } 
    else {
        comp->highScore = 1;
        comp->coins = 0;
    }

    comp->score = 1;
    comp->x = 245; comp->y = 770;           //Centro do mapa
    comp->color = WHITE;
    comp->nBalls = 1;
    comp->balls = malloc(sizeof(circle*));

    comp->balls[0] = initBall(comp->x, comp->y - 10 , 10);

    return comp;
}

//freePlayer -> Libera memoria de um jogador
player *freePlayer(player *comp){
    if (comp == NULL) return NULL;

    int i;
    for(i = 0; i<comp->nBalls; i++) 
        freeBall(comp->balls[i]);

    free(comp->balls);
    free(comp);
    return NULL;
}

//addBall -> Adiciona uma bolinha para o jogador
void addBall(player *comp){
    comp->balls = reallocarray(comp->balls, sizeof(circle), comp->nBalls+1);
    comp->balls[comp->nBalls] = initBall(comp->x, comp->y - 10, 10); 
    comp->nBalls += 1;
}

//addCoin -> Adiciona uma moeda para o jogador
void addCoin(player *comp){
    comp->coins +=1;
}

/*
    Matriz com os retangulos
    Buffer Circluar - Implementacao da <sys/queu.h>
*/

//initRow -> Inicializa uma linha com moedas, bloquinhos e powerUps
struct entry *initRow(int score){ //Temporario
    int i;
    struct entry *n1 = malloc(sizeof(struct entry));
    n1->row = calloc(7, sizeof(int)); 

    for (i=0; i<7; i++) //Quadradinhos
        n1->row[i] = (random() % 3) * score; // [0..2] * score
    
    n1->row[random() % 7] = -2;              // Moeda Bonus
    n1->row[random() % 7] = -1;              //Bolinha bonus

    return n1;
}

//freeRow -> Libera memoria de uma linha da matriz
struct entry *freeRow(struct entry *n) {
    free(n->row);
    free(n);
    return NULL;
}

/*
    Colisao entre elementos
*/

//intersectCircleSquare -> Verifica se uma bolinha colide com um quadrado
//Canto Superior Esquerdo recX, recY e Tamanho size
int intersectCircleSquare(circle *ball, float size, float recX, float recY) {  //Colisao Circulo - Retangulo
    float distX, distY, hypot;
    /* 
      recX e recY sao as posicoes na extremidade superior direita do retangulo
      Etapa1 - Verificar qual o canto mais proximo
    */
    
                                             //Bolinha a esquarda
    if (ball->x > recX + size) recX += size; //Bolinha a direita
    else if (ball->x > recX) recX = ball->x; //Bolinha no meio

                                             //Bolinha a cima
    if (ball->y > recY + size) recY += size; //Bolinha a baixo
    else if (ball->y > recY) recY = ball->y; //Bolinha no meio

   //Etapa2 - dx e dy
    distX = ball->x - recX;
    distY = ball->y - recY;

    
    //Etapa3 - Aplicar o teorema de Pitagoras
    hypot = sqrt( distX*distX + distY*distY );
    if (hypot <= ball->radius){ //Colidindo ... Onde?
        /*
            Esta parte precisa ser revisada ... c2 e c1 tem mais alguma condicao
        */
        int c1 = 0, c2 = 0;
        if (recX == ball->x) 
            ball->dy *= -1; //Baixo ou cima
        else if (recX == ball->x - distX)
            c1 = 1;

        if (recY == ball->y) 
            ball->dx *= -1;//Esq ou Dir
        else if (recY == ball->y - distY)
            c2 = 1;

        if (c2 && c1) { 
            //Cima
            ball->dx *= -1;
        }


        return 1; 
    }
    return 0;
}

//intersectCircle2 -> Verifica se uma bolinha colide com outro circulo
//Centrado em cX, cY, e de Raio rad
int intersectCircle2(circle *ball, int rad, int cX, int cY){
    float distX, distY, hypot; 
    /*
      cX, cY e rad sao, respectivamente, coordenadas do centro e seu raio
      Etapa1 - Normalizar a distancia
    */
    distX = ball->x - cX;
    distY = ball->y - cY;

    //Etapa2 - Aplicar o teorema de Pitagoras
    hypot = sqrt( distX*distX + distY*distY );
    if (hypot < ball->radius + rad)
        return 1; //Colidindo
    return 0;
}

//intersectWall -> Verifica se uma bolinha colide com umas das 4 paredes do mapa
//Caso colida com o chao: "Trava", zera dx e dy da bolinha e retorna 1
//Do contrario, retorna 0
//Caso colida com as paredes Esq ou Dir, inverte o dx
//Caso colida com o teto, inverte o dy
int intersectWall(circle *ball) { //Colisao Circulo - Parede
    float rad = ball->radius;
    if (ball->x - rad <= 0 || ball->x + rad >= 490){ //Esq ou Dir
        ball->dx *= -1; 
        if (ball->x - rad <= 0) ball->x = 0 + rad;
        else ball->x = 490 - rad;
    }

    if (ball->y - rad <= 140 ) ball->dy *= -1;                      // Teto
    else if (ball->y + rad >= 770) {                                //Chao
        ball->dx = 0; ball->dy = 0;
        ball->y = 770 - ball->radius;
        return 1;                                                   //Trava
    }

    return 0;
}

//updateBall -> Modifica dx e dy de um bolinha (EMD - Escopo mal definido);
//Caso ela tenha colidido com o chao, retorna 1
//Caso contrario, retorna 0;
int updateBall(circle *ball) { 
    int lock = intersectWall(ball);
    if (lock) ball->playing = 0;

    ball->x += ball->dx;
    ball->y += ball->dy;  
    return lock; //Travou ou n;
}

//updateCollision -> Verifica se uma bolinha colidiu com algum dos bloquinhos (EMD - Escopo mal definido);
//Identifica se colidiu ou nao com algum objeto da matriz
//Caso tenha colidido, identifica e trata o mesmo - Ex: Remove hp, toca som, inverte dx dy ...
void updateCollision(player *comp, struct circlehead *head, int n){
    struct entry *np;
    int i, j, coli; circle *ball;
        i = 1;
        ball = comp->balls[n];
        CIRCLEQ_FOREACH_REVERSE(np, head, entries){ //Linhas
            for (j=0; j<7; j++){                    //Colunas
                
                if (np->row[j] > 0){ //Bloquinho
                    coli = intersectCircleSquare(ball, 50, j*70+10, i*70+140+10);
                    if (coli){
                        al_play_sample(pop, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        np->row[j]--;
                    }
                }

                else if (np->row[j] < 0){ //PowerUp
                    coli = intersectCircle2(ball, 20, j*70 + 35, i*70 + 140 + 35);
                    if (coli){
                        switch(np->row[j]){
                            case -1:
                                addBall(comp);
                                break;
                            case -2:
                                addCoin(comp);
                                al_play_sample(coin, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                                break;
                        }
                        np->row[j] = 0; //Consome o PowerUp
                    }
                }
            } i++;
        } 

}



/*
    Desenho na matriz ixj
    E importante padronizar os tamanhos de cada elementos
    Matriz ixj = 70*i, 140 + 70*j
    Blocos = 50x50
    Bolinha = Raio(10)
    Moeda = Raio(20)
    PowerUp = Raio(20)
*/


void drawRectangle(int i, int j, int hp, ALLEGRO_FONT *font){ //hp = hitPoints
    int c=70, sp=10, l=140; //column, spacing, line

    ALLEGRO_COLOR color = al_map_rgb(255-10*hp, 220, 10*hp); //Altera a cor do bloco conforme seus hp

    al_draw_filled_rectangle(j*c+sp, i*c+l+sp  , (j+1)*c-sp, (i+1)*c+l-sp, color); //Retangulo
    al_draw_textf(font, BLACK, j*c+c/2, i*c+l+c/2, ALLEGRO_ALIGN_CENTER, "%d", hp);

}

void drawCoin(int i, int j){
    int c=70, cen=c/2,  l=140, rad=20, ir=15;//column, center, line, radius, inner radius
    al_draw_filled_circle(j*c+cen, i*c+l+cen, rad, YELLOW); //Circulo
    al_draw_filled_circle(j*c+cen, i*c+l+cen, ir, CMAP);    //Casquinha
}

void drawPowerUp(int i, int j){
    int c=70, cen=c/2,  l=140, rad=20, ir=15, b=10;//column, center, line, radius, inner radius, ball
    al_draw_filled_circle(j*c+cen, i*c+l+cen, rad, WHITE); //Circulo
    al_draw_filled_circle(j*c+cen, i*c+l+cen,  ir,  CMAP); //Casquinha
    al_draw_filled_circle(j*c+cen, i*c+l+cen,   b, WHITE); //Bolinha
}


void drawHud(ALLEGRO_FONT *font, player *comp){
    al_clear_to_color(CMAP);                            //Mapa
    al_draw_filled_rectangle(0, 0, 490, 140, CHUD);     //Retangulo Superior
    al_draw_filled_rectangle(0, 770, 490, 980, CHUD);   //Retangulo Inferiro

    al_draw_textf(font, WHITE, 490/2, 25, ALLEGRO_ALIGN_CENTRE, "%d", comp->score); //Pontuacao
    al_draw_textf(font, WHITE, 400, 25, ALLEGRO_ALIGN_LEFT, "%d", comp->coins);     //Moedas

    al_draw_filled_circle(460, 40, 10, YELLOW); //Circulo
    al_draw_filled_circle(460, 40, 5, CHUD);    //Casquinha

    al_draw_filled_rectangle(10, 30, 15, 60, CMAP); //Botao Pause
    al_draw_filled_rectangle(20, 30, 25, 60, CMAP);

    al_draw_text(font, WHITE, 40, 20, ALLEGRO_ALIGN_LEFT, "BEST"); //Pontuacao
    al_draw_textf(font, WHITE, 77, 50, ALLEGRO_ALIGN_CENTRE, "%d", comp->highScore); //Pontuacao



}

void drawMatrix(struct circlehead *head, ALLEGRO_FONT *font){
    int i = 1, j, aux;
    struct entry *np;
    CIRCLEQ_FOREACH_REVERSE(np, head, entries){ //Linhas
        for(j=0; j<7; j++){                     //Colunas
            aux = np->row[j];
            if (aux>0) //Bloco
                drawRectangle(i, j, aux, font);
            else       //PowerUp
                switch (aux){
                    case -1: //BolinhaExtra
                        drawPowerUp(i, j);
                        break;
                    case -2: //Moeda
                        drawCoin(i, j);
                        break;
                }
        } i++; //Proxima Linha
    } 
}

void drawRectangleOffset(int i, int j, int hp, ALLEGRO_FONT *font, int off){ //hp = hitPoints
    int c=70, sp=10, l=140; //column, spacing, line

    ALLEGRO_COLOR color = al_map_rgb(255-10*hp, 220, 10*hp);

    al_draw_filled_rectangle(j*c+sp, i*c+l+sp+off  , (j+1)*c-sp, (i+1)*c+l-sp+off, color); //Retangulo
    al_draw_textf(font, BLACK, j*c+c/2, i*c+l+c/2+off, ALLEGRO_ALIGN_CENTER, "%d", hp);

}

void drawCoinOffset(int i, int j, int off){
    int c=70, cen=c/2,  l=140, rad=20, ir=15;//column, center, line, radius, inner radius
    al_draw_filled_circle(j*c+cen, i*c+l+cen+off, rad, YELLOW); //Circulo
    al_draw_filled_circle(j*c+cen, i*c+l+cen+off, ir, CMAP);    //Casquinha
}

void drawPowerUpOffset(int i, int j, int off){
    int c=70, cen=c/2,  l=140, rad=20, ir=15, b=10;//column, center, line, radius, inner radius, ball
    al_draw_filled_circle(j*c+cen, i*c+l+cen+off, rad, WHITE); //Circulo
    al_draw_filled_circle(j*c+cen, i*c+l+cen+off,  ir,  CMAP); //Casquinha
    al_draw_filled_circle(j*c+cen, i*c+l+cen+off,   b, WHITE); //Bolinha
}


void drawMatrix0(struct circlehead *head, ALLEGRO_FONT *font){
    int i = 0, j, aux;
    struct entry *np;
    CIRCLEQ_FOREACH_REVERSE(np, head, entries){ //Linhas
        for(j=0; j<7; j++){                     //Colunas
            aux = np->row[j];
            if (aux>0) //Bloco
                drawRectangle(i, j, aux, font);
            else       //PowerUp
                switch (aux){
                    case -1: //BolinhaExtra
                        drawPowerUp(i, j);
                        break;
                    case -2: //Moeda
                        drawCoin(i, j);
                        break;
                }
        } i++; //Proxima Linha
    } 
}

void updateMatrixOffset(struct circlehead *head, ALLEGRO_FONT *font, int offSet){
    int i = 0, j, aux;
    struct entry *np;
    CIRCLEQ_FOREACH_REVERSE(np, head, entries){ //Linhas
        for(j=0; j<7; j++){                     //Colunas
            aux = np->row[j];
            if (aux>0) //Bloco
                drawRectangleOffset(i, j, aux, font, offSet);
            else       //PowerUp
                switch (aux){
                    case -1: //BolinhaExtra
                        drawPowerUpOffset(i, j, offSet);
                        break;
                    case -2: //Moeda
                        drawCoinOffset(i, j, offSet);
                        break;
                }
        } i++; //Proxima Linha
    } 
}

void drawBalls(player *comp){
    int n; circle *ball;
    for(n=0; n<comp->nBalls; n++){
       ball = comp->balls[n]; 
       //al_draw_circle(ball->x, ball->y, ball->radius, ball->color, 1);
       al_draw_filled_circle(ball->x, ball->y, ball->radius, comp->color);
    }
}
void drawMenu(ALLEGRO_FONT *font){
    al_clear_to_color(CMAP);

    al_draw_text(font , WHITE, 245, 210-15, ALLEGRO_ALIGN_CENTRE, "Ballz");
    al_draw_filled_rounded_rectangle(105, 490, 385, 540, 20, 20, RED);

    al_draw_text(font , WHITE, 245, 515-15, ALLEGRO_ALIGN_CENTRE, "START");
    al_draw_filled_rounded_rectangle(105, 590, 385, 640, 20, 20, BLUE);
    al_draw_text(font , WHITE, 245, 615-15, ALLEGRO_ALIGN_CENTRE, "COLOR");
}

void drawPause(ALLEGRO_FONT *font){
    al_clear_to_color(CMAP);
    al_draw_text(font , WHITE, 245, 300, ALLEGRO_ALIGN_CENTRE, "PAUSE");

    al_draw_filled_rounded_rectangle(105, 420, 385, 470, 20, 20, RED);
    al_draw_text(font , WHITE, 245, 420+10, ALLEGRO_ALIGN_CENTRE, "CONTINUE");

    al_draw_filled_rounded_rectangle(105, 490, 385, 540, 20, 20, ORRANG);
    al_draw_text(font , WHITE, 245, 490+10, ALLEGRO_ALIGN_CENTRE, "RESTART");

    al_draw_filled_rounded_rectangle(105, 560, 385, 610, 20, 20, CYAN);
    al_draw_text(font , WHITE, 245, 560+10, ALLEGRO_ALIGN_CENTRE, "MAIN MENU");
}

void drawShop(ALLEGRO_FONT *font, player *comp){
    al_clear_to_color(CMAP);
    al_draw_text(font , WHITE, 245, 240-15, ALLEGRO_ALIGN_CENTRE, "COLOR");

    
    al_draw_rectangle(150, 310, 200, 360, WHITE, 1);
    al_draw_filled_circle(150+25, 310+25, 10, RED);

    al_draw_rectangle(220, 310, 270, 360, WHITE, 1);
    al_draw_filled_circle(220+25, 310+25, 10, WHITE);

    al_draw_rectangle(290, 310, 340, 360, WHITE, 1);
    al_draw_filled_circle(290+25, 310+25, 10, CYAN);

    al_draw_rectangle(150, 380, 340, 540, WHITE, 1);//caixa com a cor escolhida
    al_draw_filled_circle(245, 460, 30, comp->color);

    al_draw_filled_rounded_rectangle(105, 560, 385, 610, 20, 20, BLUE);
    al_draw_text(font , WHITE, 245, 560+10, ALLEGRO_ALIGN_CENTRE, "BACK");


}

/*
    Funcoes Auxiliares
*/
void lineCoefficient(ALLEGRO_MOUSE_STATE state, player *comp, float *a, float *b){
    float aux, dX, dY;
    al_get_mouse_state(&state);
    dX = state.x - comp->x;
    dY = state.y - comp->y + 10;
    
    aux = sqrt(dX*dX + dY*dY);
    //Normalizando
    *a = dX/aux;
    *b = dY/aux;

    //Espelhando e escalando - a = ball->dx, b =  ball->dy;
    *a *= -10; *b *= -10;
}


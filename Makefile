PROGNAME = ballz
ALLEGRO = `pkg-config --libs allegro-5 allegro_primitives-5 allegro_font-5 allegro_ttf-5 allegro_audio-5 allegro_acodec-5`
LIBS = $(ALLEGRO) -lm 

all: main.o structs.o
	gcc -o $(PROGNAME) main.o structs.o $(LIBS) -Wall

structs.o: structs.c structs.h
	gcc -c structs.c $(LIBS) -Wall

main.o: main.c 
	gcc -c main.c $(LIBS) -Wall

clean:
	-rm -f *.o

purge: clean
	-rm -f $(PROGNAME)


#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <stdio.h>
#include "structs.h"


void playing(){
        while (1) {
            al_wait_for_event(queue, &event);
            switch (event.type){ //Logica
                case ALLEGRO_EVENT_TIMER: 

                    /*
                        THROWING - Lancamento
                    */
                    if (throwing) {                             //Lancamento
                        if (nb < comp->nBalls) {                //Uma de cada vez
                            if (tim > 10 ) {                    //A cada X frames;
                                comp->balls[nb]->playing = 1;   //Libera a proxima bolinha
                                tim = 0; nb++;
                            }   tim++;                          
                        } else throwing = 0;                   //Lancou todas as bolinhas
                    }

                    /*
                        PLAYING - Bolinhas pulando
                    */
                    if (playing) {                              //Bolinhas pulando

                        for(n=0; n<comp->nBalls; n++){         //Para todas as bolinhas
                            ball = comp->balls[n];
                            if (ball->playing){                //Se jogando, atualiza
                                anyp = 1;
                                //Checa as colisoes - COM TODOS os retangulos - Ponto pra melhoria
                                updateCollision(comp, &head, n);

                                if (updateBall(ball) && !flock){ //Se foi a primeira a cair
                                    flock = 1;
                                    comp->x = ball->x; comp->y = ball->y + ball->radius;
                                }
                            }
                                
                            
                        }//checou todas as bolinhas

                        /*
                            FIM DA RODADA?
                        */
                        if (!anyp && !throwing){ //Nenhuma bolinha quickando ou sendo jogada
                            playing = 0;
                            comp->score +=1;;
                            
                            //Logica Derrota / Proxima Linha
                            n1 = initRow(comp->score);
                            CIRCLEQ_INSERT_TAIL(&head, n1, entries);
                            tamCircleq++;

                            if (tamCircleq > 9) {
                                n1 = CIRCLEQ_FIRST(&head);
                                for (aux = 0; aux<7; aux++){
                                    if (n1->row[aux] > 0)
                                        break;
                                } 
                                CIRCLEQ_REMOVE(&head, n1, entries);
                                freeRow(n1);
                            }
                        }
                        anyp = 0; //Suponha que nenhuma bolinha esta jogando

                    }//Fim playing
                    
                    redraw = true; 
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN: //Botao de pause e mP
                    al_get_mouse_state(&state);
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_UP:         //Lancamento das bolinhas
                    if (!throwing && !playing && dY > 0) {           //Lancamento
                        throwing = 1; playing = 1;         //Inicio de um lancamento, Bolinhas em movimento
                        for(n = 0; n < comp->nBalls; n++){
                            comp->balls[n]->x = comp->x; comp->balls[n]->y = comp->y - 10 - 1;
                            comp->balls[n]->dx = a;        //P(t) = x0 + at (parametrizacao da reta)
                            comp->balls[n]->dy = b;        //P(t) = y0 + bt            """
                        }
                        tim = 0; nb = 0; flock = 0;        //timer, nBall, firstLock;
                    }
                    break;

                case ALLEGRO_EVENT_DISPLAY_CLOSE: 
                    running = 0;
                    break;
            }

            if (state.buttons && !playing) { //Calculando a inclinacao da reta
                al_get_mouse_state(&state);
                dX = state.x - comp->x;
                dY = state.y - comp->y + 10;
                
                float aux = sqrt(dX*dX + dY*dY);
                //Normalizando
                a = dX/aux;
                b = dY/aux;

                //Espelhando e escalando - a = ball->dx, b =  ball->dy;
                a *= -10; b *= -10;
            }

            if (redraw && al_is_event_queue_empty(queue)){ //Desenho
                //Interface do jogo
                drawHud(font, comp);

                //Matriz de retangulos e powerUps
                drawMatrix(&head, font);
                
                //Bolinhas
                drawBalls(comp);
                

                //Mira - Semelhanca de Triangulos
                if (state.buttons && !playing && dY > 0){ //Triangulo espelhado
                    al_draw_line(comp->x, comp->y -10,
                                 comp->x - dX, comp->y - dY,
                                 WHITE, 1);
                }

                al_flip_display();
                redraw = false;
            }
        }
}

